package de.oc.hg;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class Log4JDemo {
    private final static Logger log = Logger.getLogger(Log4JDemo.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();

        log.info("Dann mal los.");

        try {
            ((Object) null).toString();
        } catch (Exception e) {
            log.error("oh oh", e);
        }

        log.info("Ging alles glatt.");
    }
}
